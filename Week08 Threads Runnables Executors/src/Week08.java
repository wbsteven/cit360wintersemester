//https://howtodoinjava.com/java/multi-threading/task-scheduling-with-executors-scheduledthreadpoolexecutor-example/
//Got the code form this website did my best to customize it to make it more of mine but I am not good at Java


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Week08 implements Runnable {
    private String name;
    private SimpleDateFormat rand;


    public Week08(String name) {
        this.name = name;

        long time = System.currentTimeMillis();
        Date dat = new Date(time);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(dat);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.rand = format;
    }

    public String getName() {
        return name;
    }

    public void run() {
        try {
           System.out.println("Doing a task during: " + name + " - Time: " + rand);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}