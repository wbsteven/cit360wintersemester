//https://howtodoinjava.com/java/multi-threading/task-scheduling-with-executors-scheduledthreadpoolexecutor-example/
//Got the code form this website did my best to customize it to make it more of mine but I am not good at Java

import java.util.Date;
import java.util.concurrent.*;

public class ExecuteWeek08 {

    public static void main(String[] args){
            ScheduledExecutorService myService = Executors.newScheduledThreadPool(8);


            Week08 scr1 = new Week08 ("This is Task 1");
            Week08 scr2 = new Week08 ("This is Task 2");
            Week08 scr3 = new Week08 ("This is Task 3");
            Week08 scr4 = new Week08 ("This is Task 4");
            Week08 scr5 = new Week08 ("This is Task 5");
            Week08 scr6 = new Week08 ("This is Task 6");
            Week08 scr7 = new Week08 ("This is Task 7");
            Week08 scr8 = new Week08 ("This is Task 8");

            System.out.print("\n\n" + "The current time is: " + new Date() + "\n\n");

            myService.schedule(scr1, 5, TimeUnit.SECONDS);
            myService.schedule(scr2, 50, TimeUnit.SECONDS);
            myService.schedule(scr3, 10, TimeUnit.SECONDS);
            myService.schedule(scr4, 40, TimeUnit.SECONDS);
            myService.schedule(scr5, 25, TimeUnit.SECONDS);
            myService.schedule(scr6, 60, TimeUnit.SECONDS);
            myService.schedule(scr7, 3, TimeUnit.SECONDS);
            myService.schedule(scr8, 6, TimeUnit.SECONDS);

            try{
                myService.awaitTermination(1, TimeUnit.DAYS);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

    }
}