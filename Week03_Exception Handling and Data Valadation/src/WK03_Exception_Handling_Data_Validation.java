import java.util.Scanner;

public class WK03_Exception_Handling_Data_Validation {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        boolean doAgain = true;

        while(doAgain)

            try {
                System.out.println("-----Lets Divide---- ");
                System.out.println("Please enter the First Number (Numerator): ");

                int num1 = input.nextInt();

                System.out.println("Please enter the Second Number (Denominator): ");

                int num2 = input.nextInt();

                System.out.println("Your answer is:" + num1 / num2);

            } catch (ArithmeticException e) {
                System.out.println("Error:"+e);
                System.out.println("You cannot Divide by zero, please try again.");
            }
    }

}
