package entity;

//Majority of the code is from the teachers example I only
//added a insert method to add a new user to the database

import org.hibernate.Session;
import java.util.*;

public class RunTestDataBase {

    public static void main(String[] args) {

        TestDataBase t = TestDataBase.getInstance();

        List<Customer> c = t.getCustomers();
        for (Customer i : c) {
            System.out.println(i);
        }

        System.out.println(t.getCustomer(1));

        //https://howtodoinjava.com/hibernate/hibernate-insert-query-tutorial/
        //Add Customer to Data Base
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();

        //Add new Employee object
        Customer cust = new Customer();
        cust.setFirstName("Rod");
        cust.setLastName("Nockin");
        cust.setEmail("Rod.Nockin@Gmail.com");
        cust.setCity("Rock City");
        cust.setState("Alabama");
        cust.setZipCode("21991");
        cust.setPhone("605-475-6958");
        cust.setStreet("123 Sobriety Street");
        cust.setCustomerId(1002);

        //Save the employee in database
        session.save(cust);

        //Commit the transaction
        session.getTransaction().commit();
        HibernateUtils.shutdown();

    }
}