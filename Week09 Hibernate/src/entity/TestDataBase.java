package entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;


public class TestDataBase {

    SessionFactory factory = null;
    Session session = null;

    private static TestDataBase single_instance = null;

    private TestDataBase()
    {
        factory = HibernateUtils.getSessionFactory();
    }


    public static TestDataBase getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDataBase();
        }

        return single_instance;
    }


    public List<Customer> getCustomers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.Customer";
            List<Customer> cs = (List<Customer>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }


    public Customer getCustomer(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.Customer where id=" + Integer.toString(id);
            Customer c = (Customer)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }



}