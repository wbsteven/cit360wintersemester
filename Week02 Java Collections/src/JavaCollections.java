import java.util.*;

public class JavaCollections {
    public static void main(String[] args) {

        //I still dont know how to program in Java. I copied the first two from you're example and changed them up a bit

        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("Hi Donkey");
        list.add("Hello Shrek!");
        list.add("GET OUT OF MY SWAMP!");
        list.add("WHY!");
        list.add("Because you're dragon Donkeys are burning it down!");
        list.add("oh....");
        list.add("Their just kids");
        list.add("I dont care!");

        for (Object str : list) {
            System.out.println((String) str);
        }
        System.out.println("-- Queue --");
        Queue queue  = new PriorityQueue();
        queue.add("Hi Donkey");
        queue.add("Hello Shrek!");
        queue.add("GET OUT OF MY SWAMP!");
        queue.add("WHY!");
        queue.add("Because you're dragon Donkeys are burning it down!");
        queue.add("oh....");
        queue.add("Their just kids");
        queue.add("I dont care!");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        //Found this code at https://www.javatpoint.com/java-treeset
        System.out.println("-- Tree Set --");
        TreeSet set = new TreeSet();
        set.add(24);
        set.add(274);
        set.add(12);
        set.add(999);
        set.add(420);
        set.add(485);
        set.add(792);

        System.out.println("Lowest Value: "+set.pollFirst());
        System.out.println("Highest Value: "+set.pollLast());


        //https://www.geeksforgeeks.org/set-in-java/
        System.out.println("-- Set -- ");
        Set hash_Set = new HashSet();
        hash_Set.add("Hello World!");
        hash_Set.add("What is it like to be human?");
        hash_Set.add("Does it hurt to breath?");
        hash_Set.add("Do you know what its like to die?");
        hash_Set.add("How does it feel to poop?");
        hash_Set.add("Us aliens dont know anything about you?");
        hash_Set.add("Can we experiment on you?");
        hash_Set.add("Can we experiment on you?");

        System.out.println(hash_Set);





    }
}
