//This code was taken and modified from this website that I found it on
//https://www.guru99.com/junit-assert.html

package JuintTests;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AssertionTests {



        //Variable declaration
        String string1="Junit";
        String string2="Junit";
        String string3="Candy";
        String string4="Candy";
        String string5=null;
        int variable1=1;
        int	variable2=2;
        int[] airethematicArrary1 = { 1, 2, 3 };
        int[] airethematicArrary2 = { 1, 2, 3 };



    //Assert statements
    @Test
    public void testAssertEquals() {
        try {
            assertEquals(string1, string2);
            System.out.println("Equals");
           }
         catch (AssertionError e){
             System.out.println("Not equal");
             throw e;
        }
         }

    @Test
    public void testAssertSame() {
        try {
            assertSame(string3, string4);
            System.out.println("Same");
        }
        catch (AssertionError e){
                System.out.println("Not the same!");
                throw e;
            }
        }

    @Test
    public void testAssertNotSame() {
        try{
            assertNotSame(string1, string3);
            System.out.println("Not the same!");
        }
        catch (AssertionError e){
            System.out.println("They shouldn't be the same!");
            throw e;
        }
    }

    @Test
    public void testAssertNotNull() {
        try{
            assertNotNull(string1);
            System.out.println("It is Not Null");
        }
        catch (AssertionError e){
            System.out.println("It is Null");
            throw e;
        }
    }

    @Test
    public void testAssertNull() {
        try{
            assertNull(string5);
            System.out.println("It is Null");
        }
        catch (AssertionError e){
            System.out.println("It is Not Null");
            throw e;
        }
    }

    @Test
    public void testAssertTrue() {
        try{
            assertTrue(variable1<variable2);
            System.out.println("True");
        }
        catch (AssertionError e){
            System.out.println("False");
            throw e;
        }

    }

    @Test
    public void testAssertArrayEquals() {
        try{
            assertArrayEquals(airethematicArrary1, airethematicArrary2);
            System.out.println("The Arrays are the same");
        }
        catch (AssertionError e){
            System.out.println("The Arrays are not the same");
            throw e;
        }

    }

}

