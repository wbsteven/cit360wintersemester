package Stuff;

import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentTest {
    Student obj = new Student();

    @Test
    public void testDisplayStudentName() {
        Student student = new Student();
        String studentName = student.displaysStudentName("Anshuman", "Nain");
        assertEquals("AnshumanNain", studentName);
    }

    @Test
    void cube() {
        Student student = new Student();
        int output = student.cube(3);
        try {
            assertEquals(9, output);

        } catch (AssertionFailedError e) {
            System.err.println("The answer is " + e.getActual().getStringRepresentation() + " Nice work stupid!");

        }
    }



}
